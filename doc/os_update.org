#+title: Como atualizar um sistema operacional: Trisquel
#+date: 2023-03-30
#+options: toc:nil num:nil

Exemplo de atualização em [[https://trisquel.info/][Trisquel]] (sistema LTS)

Usar o comando para atualização da distribuição

#+begin_example sh
  sudo apt update
  sudo apt dist-upgrade
#+end_example

Usar o comando alternativo:

#+begin_example sh
  sudo do-release-upgrade
#+end_example

O mesmo comando, adicionando o argumento ~d~[fn:1]

#+begin_example sh
  sudo do-release-upgrade -d
#+end_example

Editar as fontes de arquivos (~sources.list~) substituindo o nome da versão
anterior do sistema (trisquel nabia no exemplo) pela nova versão (trisquel
'aramo')

#+begin_example sh
  sudo sed -i s/nabia/aramo /etc/apt/sources.list \
      && sudo apt update \
      && sudo apt full-upgrade

#+end_example


** References:

- Leia o [[https://manpages.ubuntu.com/manpages/xenial/man8/apt-get.8.html][manual de apt]]
- Verifique o processo de [[https://ubuntu.com/server/docs/upgrade-introduction][atualização]]

* Footnotes

[fn:1]Note que "~d~" vai atualizar a versão de "desenvolvimento". Isso é
pouco recomendável para sistemas que estarão em produção.
